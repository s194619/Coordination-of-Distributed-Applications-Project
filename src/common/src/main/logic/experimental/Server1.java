package common.src.main.logic.experimental;

import org.jspace.FormalField;
import org.jspace.SequentialSpace;
import org.jspace.SpaceRepository;

public class Server1 implements Runnable {

    public void run() {
        try {
            System.out.println("Init server");
            SpaceRepository repository = new SpaceRepository();

            SequentialSpace chat = new SequentialSpace();

            repository.add("chat", chat);

            repository.addGate("tcp://192.168.0.184:9001/?keep");

            while (true) {
                System.out.println("Next read");
                Object[] t = chat.get(new FormalField(String.class), new FormalField(String.class));
                System.out.println("Info: ");
                System.out.println(t[0] + ":" + t[1]);
            }
        }catch (Exception e){
            System.out.println("RIP SERVER");
        }
    }
}
