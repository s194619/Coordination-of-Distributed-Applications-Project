package common.src.main.logic.experimental;

import org.jspace.RemoteSpace;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Client1 implements Runnable{
    public String name;

    public void run() {
        System.out.println("Init client");
        try {
            RemoteSpace chat = new RemoteSpace("tcp://192.168.0.184:9001/chat?keep");
            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
            while (true) {

                String message = input.readLine();
                System.out.println("PUT");
                chat.put("name", message);
            }
        }catch (Exception e){
            System.out.println("RIP CLIENT");
        }
    }
}
