package common.src.main.logic.game;

import common.src.main.logic.core.Game;
import common.src.main.logic.core.GameObject;
import common.src.main.logic.core.Vec;

public class PhysicsObject extends GameObject {
    private Vec acceleration = new Vec();
    private Vec velocity =  new Vec();
    public float mass = 0;

    public Vec getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(Vec acceleration) {
        if (acceleration == null){
            throw new IllegalArgumentException("Acceleration cannot be null");
        }
        this.acceleration.x = acceleration.x;
        this.acceleration.y = acceleration.y;
    }

    public Vec getVelocity() {
        return velocity;
    }

    public void setVelocity(Vec velocity) {
        if (velocity == null){
            throw new IllegalArgumentException("Velocity cannot be null");
        }
        this.velocity.x = velocity.x;
        this.velocity.y = velocity.y;
    }

    @Override
    public void update() {
        super.update();
        Vec v = new Vec(acceleration);
        v.mul(Game.getDeltaTime());
        velocity.add(v);
        translate(velocity);
    }
}
