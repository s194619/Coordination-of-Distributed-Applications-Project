package common.src.main.logic.game;

import common.src.main.controller.KeyInput;
import common.src.main.controller.TankMovement;
import common.src.main.logic.core.Game;
import common.src.main.logic.core.GameObject;
import common.src.main.logic.core.Vec;

public class Tank extends GameObject {
    //KeyInput keyInput;
    TankMovement tankMovement = new TankMovement(this);

    public Tank() {
        init();
        //keyInput = new KeyInput(this);
    }

    public void shoot(PhysicsObject proj, Vec vel) {
        Vec v = new Vec(getPosition());
        Vec f = forward();
        f.mul(2);
        v.add(f);
        proj.setPosition(v);
        proj.setVelocity(vel);
    }



    @Override
    public void update() {
        super.update();
    }

}
