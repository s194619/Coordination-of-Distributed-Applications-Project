package common.src.main.logic.core;

public class CircleCollider {

    private float radius;
    private GameObject gameObject;

    public float getRadius() {
        return radius;
    }

    public CircleCollider(GameObject gameObject, float radius){
        this.gameObject = gameObject;
        this.radius = radius;
    }
    public boolean isColliding(CircleCollider c){
        Vec v = new Vec(gameObject.getPosition());
        v.sub(c.gameObject.getPosition());
        if (v.length() <= radius){
            return true;
        }
        return false;
    }
}
