package common.src.main.logic.core;

import java.net.URL;

public class Util {
    public static final String RESOURCE_PATH = "C:\\Users\\frede\\Desktop\\jSpace-Project\\resources\\";

    public final String resourcePath = "/";
    public final String fxmlPath = "/";

    public URL getResourceURL(String name) {
        return getClass().getResource(resourcePath + name);
    }
    public URL getFXMLURL(String name) {
        return getClass().getResource(fxmlPath + name);
    }


}
