package common.src.main.logic.core;

public class Game {

    private static Game instance;

    private static Game getInstance() {
        return instance;
    }

    public Game(){
        instance = this;
    }

    public void update(double et){
        elapsedTime = et;
        deltaTime = (et - prevTime);
        if (deltaTime>= 1/FPS) {
            deltaTime -= 1/FPS;
            prevTime = elapsedTime;
            for (GameObject g : GameObject.getAll()) {
                g.update();
            }
        }
    }
    public static final int FPS = 60;
    private double elapsedTime = 0, prevTime = 0, deltaTime = 0;
    public static float elapsedTime() {
        return ((float)getInstance().elapsedTime)/1000000000f;
    }
    public static float getDeltaTime() {
        return (float) getInstance().deltaTime;
    }

}
