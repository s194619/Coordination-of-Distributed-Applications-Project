package common.src.main.logic.core.observer;

public interface Obs {
    void update(Observable o);
}
