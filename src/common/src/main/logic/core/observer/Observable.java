package common.src.main.logic.core.observer;

import java.util.ArrayList;
import java.util.List;

public class Observable {
    private List<Obs> observers = new ArrayList<>();
    public void addObs(Obs o){
        observers.add(o);
    }
    public void removeObs(Obs o){
        observers.remove(o);
    }
    public void updateObs(){
        for (Obs o : observers){
            o.update(this);
        }
    }
}
