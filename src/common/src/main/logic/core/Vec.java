package common.src.main.logic.core;

public class Vec {
    public float x;
    public float y;

    public Vec(float x, float y)
    {
        this.x = x;
        this.y = y;
    }
    public Vec(Vec v)
    {
        this.x = v.x;
        this.y = v.y;
    }

    public Vec()
    {
        this.x = 0;
        this.y = 0;
    }

    public void add(Vec v){
        x += v.x;
        y += v.y;
    }
    public void sub(Vec v){
        x -= v.x;
        x -= v.x;
        y -= v.y;
    }
    public void mul(float k){
        x = k * x;
        y = k * y;
    }
    public float length(){
        return (float) Math.sqrt(x*x+y*y);
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
