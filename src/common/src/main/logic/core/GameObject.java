package common.src.main.logic.core;

import common.src.main.gui.GameObjectRenderer;
import common.src.main.logic.core.observer.Observable;

import java.util.ArrayList;
import java.util.List;

public class GameObject extends Observable {
    private Sprite sprite = new Sprite("mario.png");
    private Vec position = new Vec();
    private boolean destroyed = false;
    public boolean isDestroyed(){
        return destroyed;
    }
    private float angle = 0;

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    private static List<GameObject> all = new ArrayList<>();
    public static final List<GameObject> getAll() {
        return all;
    }

    public GameObject(){
        init();
    }
    public void init(){
        all.add(this);
        addObs(new GameObjectRenderer(this));
        updateObs();
    }
    public void destroy(){
        all.remove(this);
        destroyed = true;
        updateObs();
    }

    public final Vec getPosition() {
        return position;
    }

    public void translate(Vec v){
        position.add(v);
        updateObs();
    }

    public void setPosition(Vec v){
        if (v == null){
            throw new IllegalArgumentException("Position cannot be null");
        }
        position.x = v.x;
        position.y = v.y;
        updateObs();
    }
    public Vec forward(){
        return new Vec((float)Math.cos(Math.toRadians(angle)),(float)Math.sin(Math.toRadians(angle)));
    }

    public Sprite getSprite() {
        return sprite;
    }
    public void update()
    {

    }
}
