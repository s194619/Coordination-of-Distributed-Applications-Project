package common.src.main.controller;

import common.src.main.logic.core.GameObject;

import common.src.main.logic.core.Vec;
import common.src.main.logic.game.PhysicsObject;
import common.src.main.logic.game.Tank;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import java.net.URL;
import java.util.ResourceBundle;

public class TankMovement {
    private Tank tank;
    public EventHandler<MouseEvent> mouseClicked;
    public EventHandler<KeyEvent> keyPressed;
    public EventHandler<KeyEvent> keyReleased;
    
    public TankMovement(Tank gameObject) {
        this.tank = gameObject;

        this.setKeyPressed( new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {

                switch(event.getCode()) {
                    case W:
                        System.out.println("W");
                        tank.translate(new Vec(0,3));
                        System.out.println(tank.getPosition());
                        break;
                    case A:
                        tank.translate(new Vec(-3,0));
                        System.out.println(tank.getPosition());
                        break;
                    case S:
                        tank.translate(new Vec(0,-3));
                        System.out.println(tank.getPosition());
                        break;
                    case D:
                        tank.translate(new Vec(3,0));
                        System.out.println(tank.getPosition());
                        break;
                    case Q:
                        tank.shoot(new PhysicsObject(), new Vec(tank.forward()));
                }

            }

        });

        this.setKeyReleased( new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                switch(event.getCode()) {
                    case W:
                        tank.translate(new Vec(0.0f,0.0f));
                        break;
                    case A:
                        tank.translate(new Vec(0.0f,0.0f));
                        break;
                    case S:
                        tank.translate(new Vec(0.0f,0.0f));
                        break;
                    case D:
                        tank.translate(new Vec(0.0f,0.0f));
                        break;
                }

            }

        });


    }

    //Get/setter methods:
    public EventHandler<KeyEvent> getKeyReleased() {
        return keyReleased;
    }

    public void setKeyReleased(EventHandler<KeyEvent> keyReleased) {
        this.keyReleased = keyReleased;
    }

    public EventHandler<KeyEvent> getKeyPressed() {
        return keyPressed;
    }

    public void setKeyPressed(EventHandler<KeyEvent> keyPressed) {
        this.keyPressed = keyPressed;
    }

    public EventHandler<MouseEvent> getMouseClicked() {
        return mouseClicked;
    }

    public void setMouseClicked(EventHandler<MouseEvent> mouseClicked) {
        this.mouseClicked = mouseClicked;
    }

}
