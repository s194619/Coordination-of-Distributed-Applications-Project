package common.src.main.controller;

import common.src.main.logic.core.GameObject;
import common.src.main.logic.core.Vec;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.net.URL;
import java.util.ResourceBundle;

public class KeyInput implements Initializable {
    GameObject gameObject;

    public EventHandler<MouseEvent> MouseClicked;
    public EventHandler<KeyEvent> keyPressed;
    public EventHandler<KeyEvent> keyReleased;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("Inside Tank movement.");

        this.setKeyPressed( new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                System.out.println("MOVING.");
                switch(event.getCode()) {
                    case W:
                        gameObject.translate(new Vec(0.0f,10.0f));
                    case A:
                        gameObject.translate(new Vec(-10.0f,0.0f));
                    case S:
                        gameObject.translate(new Vec(0.0f,-10.0f));
                    case D:
                        gameObject.translate(new Vec(10.0f,0.0f));
                }

            }

        });

        this.setKeyReleased( new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                System.out.println("RELEASING MOVEMENT.");
                switch(event.getCode()) {
                    case W:
                        gameObject.translate(new Vec(0.0f,0.0f));
                    case A:
                        gameObject.translate(new Vec(0.0f,0.0f));
                    case S:
                        gameObject.translate(new Vec(0.0f,0.0f));
                    case D:
                        gameObject.translate(new Vec(0.0f,0.0f));
                }

            }

        });
    }

    public EventHandler<KeyEvent> getKeyReleased() {
        return keyReleased;
    }

    public void setKeyReleased(EventHandler<KeyEvent> keyClicked) {
        this.keyReleased = keyReleased;
    }

    public EventHandler<KeyEvent> getKeyPressed() {
        return keyPressed;
    }

    public void setKeyPressed(EventHandler<KeyEvent> keyClicked) {
        this.keyPressed = keyPressed;
    }

}
