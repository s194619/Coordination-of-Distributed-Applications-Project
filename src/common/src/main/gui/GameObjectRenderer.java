package common.src.main.gui;

import common.src.main.logic.core.GameObject;
import common.src.main.logic.core.observer.Obs;
import common.src.main.logic.core.observer.Observable;
import common.src.main.logic.game.Tank;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.transform.Rotate;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Translate;

public class GameObjectRenderer extends Group implements Obs {
    private Group group = new Group();

    private ImageView imageView = new ImageView();
    private Translate translate = new Translate();
    private Rotate rotate = new Rotate();

    private GameObject gameObject;
    public GameObjectRenderer(GameObject g){
        gameObject = g;
        g.addObs(this);
        App.getInstance().getRoot().getChildren().add(this);
        //getModelObject(g, group);


        getChildren().add(group);
        if (gameObject.getSprite() != null) {
            //imageView.setImage(new Image(gameObject.getSprite().getPath()));
        }


        group.getChildren().add(imageView);
        getTransforms().add(translate);

        rotate.setAxis(Rotate.Z_AXIS);
        getTransforms().add(rotate);
    }

    public void getModelObject(GameObject gameObject, Group group) {
        Object object = gameObject;

        if(gameObject instanceof Tank) {
            Rectangle r2 = new Rectangle(18, 18, Color.rgb(54,166,15));
            r2.setStroke(Color.BLACK);
            //r2.setFill(null);
            r2.setStrokeWidth(3);
            r2.setArcWidth(25);
            r2.setArcHeight(25);
            group.getChildren().add(r2);
            group.getChildren().get(0).setTranslateX(200);
            group.getChildren().get(0).setTranslateY(200);
        } else {
            Circle circ = new Circle(12, Color.rgb(0,0,0));
            group.getChildren().add(circ);
            group.getChildren().get(0).setTranslateX(200);
            group.getChildren().get(0).setTranslateY(200);

        }

    }

    @Override
    public void update(Observable o) {
        translate.setX(gameObject.getPosition().x);
        translate.setY(-gameObject.getPosition().y);
        rotate.setAngle(gameObject.getAngle());
        //if (!imageView.getImage().getUrl() != gameObject.getSprite().getPath())
        //System.out.println(translate.getX() + "_" + translate.getY());
        if (gameObject.isDestroyed()){
            gameObject.removeObs(this);
            App.getInstance().getRoot().getChildren().remove(this);
        }
    }
}
