package common.src.main.gui;


import common.src.main.controller.KeyInput;
import common.src.main.controller.TankMovement;
import common.src.main.gui.GameObjectRenderer;
import common.src.main.logic.core.Game;
import common.src.main.logic.core.GameObject;
import common.src.main.logic.core.Util;
import common.src.main.logic.core.Vec;
import common.src.main.logic.game.PhysicsObject;
import common.src.main.logic.game.Tank;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class App extends Application {

    //Field variables:
    static private App instance;
    private KeyInput keyInput;
    private Util util = new Util();
    private Stage window;
    private Game game;
    private Group root;
    private Tank tank;
    private TankMovement tankMovement;
    private Scene scene;
    @FXML private AnchorPane mainPane;

    //Getter methods:
    public static App getInstance(){
        return instance;
    }

    public Group getRoot() {
        return root;
    }

    public Game getGame() {
        return game;
    }

    //Main method:
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException, InterruptedException {

        instance = this;
        game = new Game();
        window = primaryStage;
        primaryStage.setTitle("Hello World!");
        instantiateScene("main.fxml");

        //For implementation purposes:
        startGame();
        scene.setOnKeyPressed(tankMovement.keyPressed);
        scene.setOnKeyReleased(tankMovement.keyReleased);

    }

    public void instantiateScene(String pathFXML) {
        try {
            Parent rootTest = FXMLLoader.load(getClass().getResource("main.fxml"));
            scene = new Scene(rootTest, 400, 400);
            scene.getStylesheets().add(getClass()
                    .getResource("application.css").toExternalForm());
            window.setScene(scene);
            window.show();

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void startGame() {

        root = new Group();

        AnimationTimer gameLoop = new AnimationTimer()
        {
            float startTime;
            boolean hasStarted = false;
            public void handle(long currTime)
            {
                if (!hasStarted){
                    startTime = currTime;
                    hasStarted = true;
                }
                game.update((currTime-startTime)/1000000000d);
            }
        };
        gameLoop.start();
        //Setup scene:
        scene = new Scene(root, 400, 400);
        window.setScene(scene);
        window.show();

        //Add objects:
        tank = new Tank();
        tankMovement = new TankMovement(tank);
    }

}
